; Drupal.org release file.
core = 7.x
api = 2

; sunspots.
;
projects[libraries][version] = 2.1
projects[libraries][subdir] = contrib
; Patch to allow profiles to search inherited profile libraries.
projects[libraries][patch][1783598] = http://drupal.org/files/libraries-profile_inheritance-1783598-4.patch

; themes.
;
projects[adaptivetheme][version] = 3.1
projects[adaptivetheme][subdir] = contrib

projects[radix][version] = 2.0-beta5
projects[radix][subdir] = contrib

; Commerce.
;
projects[commerce][version] = 1.8
projects[commerce][subdir] = commerce
projects[commerce][patch][] = http://drupal.org/files/undefined-hostname.patch

; Commerce Backoffice.
;
projects[commerce_backoffice][version] = 1.3
projects[commerce_backoffice][subdir] = commerce

; Commerce Flat Rate
;
projects[commerce_flat_rate][version] = 1.0-beta2
projects[commerce_flat_rate][subdir] = commerce

; Commerce Message.
;
projects[commerce_message][version] = 1.0-rc1
projects[commerce_message][subdir] = commerce

; Commerce Paypal.
;
projects[commerce_paypal][version] = 2.2
projects[commerce_paypal][subdir] = commerce

; Commerce Physical.
;
projects[commerce_physical][version] = 1.x-dev
projects[commerce_physical][subdir] = commerce

; Commerce Shipping.
;
projects[commerce_shipping][version] = 2.0
projects[commerce_shipping][subdir] = commerce

; Commerce USPS.
;
projects[commerce_usps][version] = 2.x-dev
projects[commerce_usps][subdir] = commerce
