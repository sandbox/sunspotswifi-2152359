api = 2
core = 7.x

; Drupal Core
projects[drupal][version] = 7.24
projects[drupal][type] = core

; Patch to allow profile inheritance.
projects[drupal][patch][1356276] = http://drupal.org/files/1356276-inheritable-profiles_0.patch
