api = 2
core = 7.x

; Include the definition for how to build Drupal core directly, including patches:
includes[] = drupal-org-core.make

; Panopoly.
projects[panopoly][type] = profile 
projects[panopoly][download][type] = git
projects[panopoly][download][branch] = 7.x-1.x

; Download and install Sunspots.
projects[sunspots][type] = profile
projects[sunspots][download][type] = git
projects[sunspots][download][branch] = 7.x-1.x
;projects[sunspots][download][revision] = 70ff20c5d0d4f3feb2eee3809d0874ef7978073f
projects[sunspots][download][url] = http://git.drupal.org/sandbox/sunspotswifi/2152359.git

