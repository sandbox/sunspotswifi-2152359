<?php
/**
 * @file
 * The Sunspots WiFi website installation profile.
 *
 * Installs and configures the base Sunspots WiFi website.
 */
require_once ("profiles/panopoly/panopoly.profile");

/**
 * Implements hook_install_tasks()
 */
function sunspots_install_tasks(&$install_state) {
  $tasks = panopoly_install_tasks($install_state);
  return $tasks;
}

/**
 * Implements hook_install_tasks_alter()
 */
function sunspots_install_tasks_alter(&$tasks, $install_state) {
  panopoly_install_tasks_alter($tasks, $install_state);
}

/**
 * Implements hook_form_FORM_ID_alter()
 */
function sunspots_form_install_configure_form_alter(&$form, $form_state) {
  panopoly_form_install_configure_form_alter($form, $form_state);
}

/**
 * Implements hook_form_FORM_ID_alter()
 */
function sunspots_form_apps_profile_apps_select_form_alter(&$form, $form_state) {
  panopoly_form_apps_profile_apps_select_form_alter($form, $form_state);
}
